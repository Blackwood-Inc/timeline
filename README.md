![Timeline](https://bytebucket.org/Blackwood-Inc/timeline/raw/61c8ee745a70c1ee1de3fcd5aebdf38cd13a2c34/Icons/title.png "Title")

Welcome to the Timeline repository. Timeline is an open source application that helps writers keep track of their character's backstories and worlds. 


# Index

1. [Vision Statement](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-vision-statement)
1. [Why Timeline?](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-why-timeline)
1. [Learn More](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-learn-more)
1. [Who Am I?](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-who-am-i)
1. [Contributing](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-contributing)
    * [Getting Started](https://bitbucket.org/blackwood-inc/timeline/overview#markdown-header-getting-started)



## Vision Statement

> For writers who have characters with detailed and complex backstories, Timeline is a mobile phone application that can store information on your characters in an easy to use structure. 
The app allows you to track characters throughout their entire lives, with details down to the second. It is also extensible, allowing you to add new features as you see fit. Along with 
characteristics, this application can also track groups that the character belongs to and their inventory. Unlike other character making applications, this app allows for far greater detail
in background creation.  

Source: NA

Web Page: NA


## Why Timeline?

While there are plenty of resources out there for helping you keep track of your stories, I wasn't able to find one that really did it the way I wanted to. Plenty had good information for character (though 
usually were lacking in how detailed they got) but none had a great way to making complex backstories. So, I decided to make one myself. Though I'm sure the first few iterations will fall short of my goal, 
Timeline is meant to be fully customizable and able to track down every detail of your characters life. In the future 


## Learn More

If you have any questions about the app, or suggestions on changes, email me at wleingang15@mail.wou.edu. I'll get sure to get back to you, one way or another. 


# Who Am I?

#### William Leingang

> I'm a software engineer with experience in quite a few languages. While I'm primarily programmer, my real passion is writing. I try to merge the two as often as possible, whether that be in 
> coding projects such as Timeline, or trying to get story prompts to be generated through markov chains. 
>  
> Through an REU (Research Experience for Undergraduates) I became experienced in machine learning techniques. 
> 
> I plan to graduate in Spring of 2018 with a Bachelors in Computer Science from Western Oregon University. 


# Contributing

Timeline is an open source project, so anyone can contribute. However, I will not be accepting pull requests until after June 15th, 2018. At this point, the first release of the application will be finished
and the project opened to public contributions. If you want to help us by adding to our codebase or fixing bugs at that time, please feel free to. Once again, if you have any questions, send them wleingang15@mail.wou.edu.

When contributing to this file, please add your name to the [contributors file](ADD LATER).


## Getting Started

If this is your first time coding using a forked repository and pull requests to contribute to a project, I highly recommend using 
[this](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) tutorial. It walks you through step by step instructions on everything from initially forking the repository to
making your first pull request. 
