package com.a.timeline;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.button);


        ArrayList<World> worlds = new ArrayList<>();

        worlds.add(new World("TEST"));

        ctw.bind("cn=ArrayList", worlds);

        for (World w:worlds) {
            final Button myButton = new Button(this);
            myButton.setText(w.getName());
            ViewGroup layout = findViewById(R.id.main_layout);
            layout.addView(myButton);
        }




        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


            }
        });
    }
}
